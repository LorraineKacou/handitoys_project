package com.handi.handitoys;


import com.handi.handitoys.entity.Toy;
import com.handi.handitoys.entity.User;
import com.handi.handitoys.repository.ToyRepository;
import com.handi.handitoys.repository.UserRepository;
import com.handi.handitoys.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
public class UserServiceIntegrationTest {

    @Autowired
    UserService userServiceInstance;
    @Autowired
    private UserRepository userRepositoryInstance;


    private User userSample;


    @Before
    public void setUp() {
        // clean before each test
        this.userRepositoryInstance.deleteAll();


        this.userSample = this.userRepositoryInstance
                .save(new User(null, "lastname","firstname","email","password","role",new ArrayList<>(),new ArrayList<>()));

    }


    @Test
    public void whenCreateThenReturnNewUser(){

        User user = new User(null,"lastname","firstname","email","password","role",new ArrayList<>(),new ArrayList<>() );

        User userCreated = userServiceInstance.createUser(user);

        assertNotNull(userCreated);
        assertNotNull(userCreated.getUserId());


    }
}
