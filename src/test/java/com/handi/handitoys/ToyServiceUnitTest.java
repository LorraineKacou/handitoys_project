package com.handi.handitoys;

import com.handi.handitoys.entity.Toy;
import com.handi.handitoys.repository.ToyRepository;
import com.handi.handitoys.service.ToyService;
import com.handi.handitoys.service.ToyServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ToyServiceUnitTest {
    private final ToyRepository toyRepositoryMock;
    private final ToyService toyServiceInstance;

    public ToyServiceUnitTest(){
        this.toyRepositoryMock=Mockito.mock(ToyRepository.class);
        this.toyServiceInstance=new ToyServiceImpl(toyRepositoryMock);
    }

    @Before
    public void setUp() {
        Toy toy = new Toy(1L, "image", "description", "title", new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        Mockito.when(toyRepositoryMock.findAll()).thenReturn(List.of(toy));
    }

    @Test
    public void testGetAll(){
        List<Toy> toyList= toyServiceInstance.getAllToys();
        assertEquals(1,toyList.size());
    }
}
