package com.handi.handitoys.repository;

import com.handi.handitoys.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
