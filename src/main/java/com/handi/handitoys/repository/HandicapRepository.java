package com.handi.handitoys.repository;

import com.handi.handitoys.entity.Handicap;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HandicapRepository extends JpaRepository<Handicap, Long> {
}
