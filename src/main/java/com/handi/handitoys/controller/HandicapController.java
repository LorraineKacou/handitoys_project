package com.handi.handitoys.controller;


import com.handi.handitoys.entity.Handicap;
import com.handi.handitoys.service.HandicapService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@RestController
@RequestMapping("handicaps")
public class HandicapController {
    private final HandicapService handicapServiceInstance;

    public HandicapController(HandicapService handicapServiceInstance) {
        this.handicapServiceInstance = handicapServiceInstance;
    }

    @GetMapping()
    public List<Handicap> getAllHandicaps(){
        return handicapServiceInstance.getAllHandicaps();
    }


    @PostMapping()
    public ResponseEntity<Handicap> createHandicap(@RequestBody Handicap handicap){
        Handicap newHandicap = handicapServiceInstance.createHandicap(handicap);
        return new ResponseEntity<>(newHandicap, HttpStatus.CREATED);

    }

    @PutMapping("/{handicapId}")
    public ResponseEntity<Handicap> updateHandicap(@PathVariable Long handicapId,
                                         @RequestBody Handicap handicapInfos) {
        Handicap handicapUpdated = handicapServiceInstance.updateHandicap(handicapId, handicapInfos);
        if (handicapUpdated == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "No handicap found with id " + handicapId);
        } else {
            return new ResponseEntity<>(handicapUpdated, HttpStatus.OK);
        }
    }

    @DeleteMapping("/{handicapId}")
    public ResponseEntity<Void> deleteOneHandicap(@PathVariable Long handicapId) {
        handicapServiceInstance.deleteHandicap(handicapId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
