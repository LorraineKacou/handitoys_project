package com.handi.handitoys.controller;


import com.handi.handitoys.entity.User;
import com.handi.handitoys.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("users")
public class UserController {
    private final UserService userServiceInstance;

    public UserController(UserService userServiceInstance) {
        this.userServiceInstance = userServiceInstance;
    }

    @GetMapping()
    public List<User> getAllUsers() {
        return userServiceInstance.getAllUsers();
    }


    @PostMapping()
    public ResponseEntity<User> createUser(@RequestBody User user) {
        User newUser = userServiceInstance.createUser(user);
        return new ResponseEntity<>(newUser, HttpStatus.CREATED);

    }

    @PutMapping("/{userId}")
    public ResponseEntity<User> updateUser(@PathVariable Long userId,
                                           @RequestBody User userInfos) {
        User userUpdated = userServiceInstance.updateUser(userId, userInfos);
        if (userUpdated == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "No user found with id " + userId);
        } else {
            return new ResponseEntity<>(userUpdated, HttpStatus.OK);
        }
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<Void> deleteOneUser(@PathVariable Long userId) {
        userServiceInstance.deleteUser(userId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}
