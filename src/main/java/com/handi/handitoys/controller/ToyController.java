package com.handi.handitoys.controller;

import com.handi.handitoys.entity.Toy;
import com.handi.handitoys.service.ToyService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("toys")
@CrossOrigin(origins = "http://localhost:5173")
public class ToyController {
    private final ToyService toyServiceInstance;

    public ToyController(ToyService toyServiceInstance) {
        this.toyServiceInstance = toyServiceInstance;
    }

    @GetMapping()
    public List<Toy> getAllToys() {
        return toyServiceInstance.getAllToys();
    }


    @PostMapping()
    public ResponseEntity<Toy> createToy(@RequestBody Toy toy) {
        Toy newToy = toyServiceInstance.createToy(toy);
        return new ResponseEntity<>(newToy, HttpStatus.CREATED);

    }

    @PutMapping("/{toyId}")
    public ResponseEntity<Toy> updateToy(@PathVariable Long toyId,
                                         @RequestBody Toy toyInfos) {
        Toy toyUpdated = toyServiceInstance.updateToy(toyId, toyInfos);
        if (toyUpdated == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "No toy found with id " + toyId);
        } else {
            return new ResponseEntity<>(toyUpdated, HttpStatus.OK);
        }
    }

    @DeleteMapping("/{toyId}")
    public ResponseEntity<Void> deleteOneToy(@PathVariable Long toyId) {
        toyServiceInstance.deleteToy(toyId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
