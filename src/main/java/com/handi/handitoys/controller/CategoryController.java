package com.handi.handitoys.controller;

import com.handi.handitoys.entity.Category;
import com.handi.handitoys.service.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("categories")
public class CategoryController {
    private final CategoryService categoryServiceInstance;

    public CategoryController(CategoryService categoryServiceInstance) {
        this.categoryServiceInstance = categoryServiceInstance;
    }

    @GetMapping()
    public List<Category> getAllCategories() {
        return categoryServiceInstance.getAllCategories();
    }


    @PostMapping()
    public ResponseEntity<Category> createCategory(@RequestBody Category category) {
        Category newCategory = categoryServiceInstance.createCategory(category);
        return new ResponseEntity<>(newCategory, HttpStatus.CREATED);

    }

    @PutMapping("/{categoryId}")
    public ResponseEntity<Category> updateCategory(@PathVariable Long categoryId,
                                                   @RequestBody Category categoryInfos) {
        Category categoryUpdated = categoryServiceInstance.updateCategory(categoryId, categoryInfos);
        if (categoryUpdated == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "No category found with id " + categoryId);
        } else {
            return new ResponseEntity<>(categoryUpdated, HttpStatus.OK);
        }
    }

    @DeleteMapping("/{categoryId}")
    public ResponseEntity<Void> deleteOneCategory(@PathVariable Long categoryId) {
        categoryServiceInstance.deleteCategory(categoryId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

