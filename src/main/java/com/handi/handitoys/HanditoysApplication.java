package com.handi.handitoys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HanditoysApplication {

	public static void main(String[] args) {
		SpringApplication.run(HanditoysApplication.class, args);
	}

}
