package com.handi.handitoys.service;


import com.handi.handitoys.entity.Handicap;
import com.handi.handitoys.repository.HandicapRepository;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HandicapServiceImpl implements HandicapService{
    private final HandicapRepository handicapRepositoryInstance;

    public HandicapServiceImpl(HandicapRepository handicapRepositoryInstance) {
        this.handicapRepositoryInstance = handicapRepositoryInstance;
    }

    @Override
    public List<Handicap> getAllHandicaps() {
        return handicapRepositoryInstance.findAll();
    }

    @Override
    public Handicap createHandicap(Handicap handicapInstance) {
        return handicapRepositoryInstance.save(handicapInstance);
    }
    @Override
    public Handicap updateHandicap(Long handicapId,
                         Handicap handicapInfos) {
        // charger l'élément en base de données
        Optional<Handicap> handicapOptional = handicapRepositoryInstance
                .findById(handicapId);

        if (handicapOptional.isPresent()) {
            Handicap handicapInstance = handicapOptional.get();

            // modifier cet élément
            if (handicapInfos.getName() != null) {
                handicapInstance.setName(handicapInfos.getName());
            }


            return handicapRepositoryInstance.save(handicapInstance);
        } else {
            return null;
        }
    }
    @Override
    public void deleteHandicap(Long handicapId) {
        handicapRepositoryInstance.deleteById(handicapId);
    }

}
