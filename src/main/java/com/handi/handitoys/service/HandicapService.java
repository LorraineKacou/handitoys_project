package com.handi.handitoys.service;

import com.handi.handitoys.entity.Handicap;


import java.util.List;

public interface HandicapService {
    List<Handicap> getAllHandicaps();

    Handicap createHandicap(Handicap handicapInstance);

    Handicap updateHandicap(Long handicapId, Handicap handicapInfos);
    void deleteHandicap(Long handicapId);
}
