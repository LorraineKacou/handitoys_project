package com.handi.handitoys.service;

import com.handi.handitoys.entity.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();

    User createUser(User userInstance);

    User updateUser(Long userId, User userInfos);
    void deleteUser(Long userId);
}
