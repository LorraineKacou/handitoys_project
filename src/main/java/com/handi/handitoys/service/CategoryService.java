package com.handi.handitoys.service;

import com.handi.handitoys.entity.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAllCategories();

    Category createCategory(Category categoryInstance);

    Category updateCategory(Long categoryId, Category categoryInfos);
    void deleteCategory(Long categoryId);
}
