package com.handi.handitoys.service;

import com.handi.handitoys.entity.Category;
import com.handi.handitoys.repository.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepositoryInstance;

    public CategoryServiceImpl(CategoryRepository categoryRepositoryInstance) {
        this.categoryRepositoryInstance = categoryRepositoryInstance;
    }

    @Override
    public List<Category> getAllCategories() {
        return categoryRepositoryInstance.findAll();
    }

    @Override
    public Category createCategory(Category categoryInstance) {
        return categoryRepositoryInstance.save(categoryInstance);
    }
    @Override
    public Category updateCategory(Long categoryId,
                         Category categoryInfos) {
        // charger l'élément en base de données
        Optional<Category> categoryOptional = categoryRepositoryInstance
                .findById(categoryId);

        if (categoryOptional.isPresent()) {
            Category categoryInstance = categoryOptional.get();

            // modifier cet élément
            if (categoryInfos.getTitle() != null) {
                categoryInstance.setTitle(categoryInfos.getTitle());
            }

            if (categoryInfos.getDescription() != null) {
                categoryInstance.setDescription(categoryInfos.getDescription());
            }


            return categoryRepositoryInstance.save(categoryInstance);
        } else {
            return null;
        }
    }
    @Override
    public void deleteCategory(Long categoryId) {
        categoryRepositoryInstance.deleteById(categoryId);
    }

}

