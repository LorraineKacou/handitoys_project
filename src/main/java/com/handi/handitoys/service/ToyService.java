package com.handi.handitoys.service;

import com.handi.handitoys.entity.Toy;

import java.util.List;

public interface ToyService {

    List<Toy> getAllToys();

    Toy createToy(Toy toyInstance);

    Toy updateToy(Long toyId, Toy toyInfos);
    void deleteToy(Long toyId);
}
