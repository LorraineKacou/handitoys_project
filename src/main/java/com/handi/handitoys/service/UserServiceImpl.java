package com.handi.handitoys.service;

import com.handi.handitoys.entity.User;
import com.handi.handitoys.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService  {

    private final UserRepository userRepositoryInstance;

    public UserServiceImpl(UserRepository userRepositoryInstance) {
        this.userRepositoryInstance = userRepositoryInstance;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepositoryInstance.findAll();
    }

    @Override
    public User createUser(User userInstance) {
        return userRepositoryInstance.save(userInstance);
    }
    @Override
    public User updateUser(Long userId,
                                   User userInfos) {
        // charger l'élément en base de données
        Optional<User> userOptional = userRepositoryInstance
                .findById(userId);

        if (userOptional.isPresent()) {
            User userInstance = userOptional.get();

            // modifier cet élément
            if (userInfos.getLastname() != null) {
                userInstance.setLastname(userInfos.getLastname());
            }

            if (userInfos.getFirstname() != null) {
                userInstance.setFirstname(userInfos.getFirstname());
            }

            if (userInfos.getEmail() != null) {
                userInstance.setEmail(userInfos.getEmail());
            }

            if (userInfos.getPassword() != null) {
                userInstance.setPassword(userInfos.getPassword());
            }

            if (userInfos.getRole() != null) {
                userInstance.setRole(userInfos.getRole());
            }
            return userRepositoryInstance.save(userInstance);
        } else {
            return null;
        }
    }
    @Override
    public void deleteUser(Long userId) {
        userRepositoryInstance.deleteById(userId);
    }



}
