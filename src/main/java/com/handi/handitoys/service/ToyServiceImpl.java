package com.handi.handitoys.service;

import com.handi.handitoys.entity.Toy;

import com.handi.handitoys.repository.ToyRepository;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ToyServiceImpl implements ToyService {
    private final ToyRepository toyRepositoryInstance;

    public ToyServiceImpl(ToyRepository toyRepositoryInstance) {
        this.toyRepositoryInstance = toyRepositoryInstance;
    }

    @Override
    public List<Toy> getAllToys() {
        return toyRepositoryInstance.findAll();
    }

    @Override
    public Toy createToy(Toy toyInstance) {
        return toyRepositoryInstance.save(toyInstance);
    }
    @Override
    public Toy updateToy(Long toyId,
                           Toy toyInfos) {
        // charger l'élément en base de données
        Optional<Toy> toyOptional = toyRepositoryInstance
                .findById(toyId);

        if (toyOptional.isPresent()) {
            Toy toyInstance = toyOptional.get();

            // modifier cet élément
            if (toyInfos.getImage() != null) {
                toyInstance.setImage(toyInfos.getImage());
            }

            if (toyInfos.getDescription() != null) {
                toyInstance.setDescription(toyInfos.getDescription());
            }

            if (toyInfos.getTitle() != null) {
                toyInstance.setTitle(toyInfos.getTitle());
            }


            return toyRepositoryInstance.save(toyInstance);
        } else {
            return null;
        }
    }
    @Override
    public void deleteToy(Long toyId) {
        toyRepositoryInstance.deleteById(toyId);
    }

}
