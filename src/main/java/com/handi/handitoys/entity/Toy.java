package com.handi.handitoys.entity;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "toy")
public class Toy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long toyId;
    @Column(nullable = false)
    private String image;
    @Column(nullable = false)
    private String description;
    @Column(nullable = false)
    private String title;

    @ManyToMany(mappedBy = "userToyList")
    private List<User> userList = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "toy_has_category",
            joinColumns = @JoinColumn(name = "toyId"),
            inverseJoinColumns = @JoinColumn(name = "categoryId"))
    private List<Category> toyCategoryList = new ArrayList<>();


    @ManyToMany(mappedBy = "handicapToyList")
    private List<Handicap> handicapList = new ArrayList<>();

    public Toy() {

    }

    public Toy(Long toyId, String image, String description, String title, List<User> userList, List<Category> toyCategoryList, List<Handicap> handicapList) {
        this.toyId = toyId;
        this.image = image;
        this.description = description;
        this.title = title;
        this.userList = userList;
        this.toyCategoryList = toyCategoryList;
        this.handicapList = handicapList;
    }

    public Long getToyId() {
        return toyId;
    }

    public void setToyId(Long toyId) {
        this.toyId = toyId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
