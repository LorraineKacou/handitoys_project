package com.handi.handitoys.entity;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "handicap")
public class Handicap {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long handicapId;
    @Column(nullable = false)
    private String name;

    @ManyToMany(mappedBy = "userHandicapList")
    private List<User> userList = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "handicap_has_toy",
            joinColumns = @JoinColumn(name = "handicapId"),
            inverseJoinColumns = @JoinColumn(name = "toyId"))
    private List<Toy> handicapToyList = new ArrayList<>();


    public Handicap() {

    }

    public Long getHandicapId() {
        return handicapId;
    }

    public void setHandicapId(Long handicapId) {
        this.handicapId = handicapId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
